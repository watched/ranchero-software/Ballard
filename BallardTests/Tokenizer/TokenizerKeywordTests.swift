//
//  TokenizerKeywordTests.swift
//  BallardTests
//
//  Created by Brent Simmons on 11/19/18.
//  Copyright © 2018 Ranchero Software. All rights reserved.
//

import XCTest
@testable import Ballard

class TokenizerKeywordTests: XCTestCase {

	func testKeywords() {
		let keywords: [String: TokenType] = ["for": .forToken, "in": .inToken, "while": .whileToken, "if": .ifToken, "else": .elseToken, "try": .tryToken, "block": .block, "break": .breakToken, "continue": .continueToken, "return": .returnToken, "let": .letToken, "var": .varToken, "def": .def, "true": .trueToken, "false": .falseToken]

		keywords.forEach { (key, tokenType) in
			let tokens = try! Tokenizer.tokens(for: key)
			XCTAssertEqual(tokens.count, 1)
			let token = tokens.first!
			compareKeywordToken(token, tokenType, TextPosition(0, 0))
		}
	}

	func testKeywordsPerformance() {
		self.measure {
			testKeywords()
		}
	}
}

private extension TokenizerKeywordTests {

	func compareKeywordToken(_ token: Token, _ expectedTokenType: TokenType, _ expectedTextPosition: TextPosition) {
		XCTAssertEqual(token.type, expectedTokenType)
		XCTAssertEqual(token.textPosition, expectedTextPosition)
	}
}
