//
//  TokenizerNumberTests.swift
//  BallardTests
//
//  Created by Brent Simmons on 11/18/18.
//  Copyright © 2018 Ranchero Software. All rights reserved.
//

import XCTest
@testable import Ballard

class TokenizerNumberTests: XCTestCase {

	func testSimpleInt1() {
		let s = "1"
		let tokens = try! Tokenizer.tokens(for: s)
		XCTAssertEqual(tokens.count, 1)
		let token = tokens.first!
		compareIntToken(token, 1, TextPosition(0, 0))
	}

	func testSimpleInt2() {
		let s = "30672458"
		let tokens = try! Tokenizer.tokens(for: s)
		XCTAssertEqual(tokens.count, 1)
		let token = tokens.first!
		compareIntToken(token, 30672458, TextPosition(0, 0))
	}

	func testSimpleDouble1() {
		let s = "3.14"
		let tokens = try! Tokenizer.tokens(for: s)
		XCTAssertEqual(tokens.count, 1)
		let token = tokens.first!
		compareDoubleToken(token, 3.14, TextPosition(0, 0))
	}

	func testSimpleDouble2() {
		let s = "3067.2458"
		let tokens = try! Tokenizer.tokens(for: s)
		XCTAssertEqual(tokens.count, 1)
		let token = tokens.first!
		compareDoubleToken(token, 3067.2458, TextPosition(0, 0))
	}

	func testSimpleHexValue1() {
		let s = "0x38AF3"
		let tokens = try! Tokenizer.tokens(for: s)
		XCTAssertEqual(tokens.count, 1)
		let token = tokens.first!
		compareIntToken(token, 0x38AF3, TextPosition(0, 0))
	}

	func testSimpleHexValue2() {
		let s = "0x0000000100a2bb40"
		let tokens = try! Tokenizer.tokens(for: s)
		XCTAssertEqual(tokens.count, 1)
		let token = tokens.first!
		compareIntToken(token, 0x0000000100a2bb40, TextPosition(0, 0))
	}

	func testIntPerformance() {
		let s = "30672458"
		self.measure {
			let _ = try! Tokenizer.tokens(for: s)
		}
	}

	func testDoublePerformance() {
		let s = "3067.2458"
		self.measure {
			let _ = try! Tokenizer.tokens(for: s)
		}
	}

	func testHexPerformance() {
		let s = "0x0000000100a2bb40"
		self.measure {
			let _ = try! Tokenizer.tokens(for: s)
		}
	}
}

private extension TokenizerNumberTests {

	func compareIntToken(_ token: Token, _ expectedValue: Int, _ expectedTextPosition: TextPosition) {
		XCTAssertEqual(token.type, TokenType.literalInt)
		XCTAssertEqual(token.value! as! Int, expectedValue)
		XCTAssertEqual(token.textPosition, expectedTextPosition)
	}

	func compareDoubleToken(_ token: Token, _ expectedValue: Double, _ expectedTextPosition: TextPosition) {
		XCTAssertEqual(token.type, TokenType.literalDouble)
		XCTAssertEqual(token.value! as! Double, expectedValue, accuracy: 0.01)
		XCTAssertEqual(token.textPosition, expectedTextPosition)
	}
}
