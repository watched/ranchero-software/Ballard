//
//  TokenizerPunctuationTests.swift
//  BallardTests
//
//  Created by Brent Simmons on 11/19/18.
//  Copyright © 2018 Ranchero Software. All rights reserved.
//

import XCTest
@testable import Ballard

class TokenizerPunctuationTests: XCTestCase {

	func testPunctuation() {
		let punctuations: [String: TokenType] = ["!": .not, "&&": .and, "||": .or, "==": .equals, "!=": .notEquals, ">": .greaterThan, "<": .lessThan, ">=": .greaterThanEquals, "<=": .lessThanEquals, "=": .assign, "+": .plus, "-": .minus, "*": .star, "/": .slash, "%": .percent, "++": .plusPlus, "--": .minusMinus, ",": .comma, "(": .leftParen, ")": .rightParen, ";": .semicolon, "{": .leftBrace, "}": .rightBrace, ".": .dot, ":": .colon, "[": .leftBracket, "]": .rightBracket, "@": .at, "^": .caret]

		punctuations.forEach { (key, tokenType) in
			let tokens = try! Tokenizer.tokens(for: key)
			XCTAssertEqual(tokens.count, 1)
			let token = tokens.first!
			comparePunctuationToken(token, tokenType, TextPosition(0, 0))
		}
	}

	func testPunctuationPerformance() {
		self.measure {
			testPunctuation()
		}
	}
}

private extension TokenizerPunctuationTests {

	func comparePunctuationToken(_ token: Token, _ expectedTokenType: TokenType, _ expectedTextPosition: TextPosition) {
		XCTAssertEqual(token.type, expectedTokenType)
		XCTAssertEqual(token.textPosition, expectedTextPosition)
	}
}
