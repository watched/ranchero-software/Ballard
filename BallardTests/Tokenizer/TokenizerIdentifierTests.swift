//
//  TokenizerIdentifierTests.swift
//  BallardTests
//
//  Created by Brent Simmons on 11/19/18.
//  Copyright © 2018 Ranchero Software. All rights reserved.
//

import XCTest
@testable import Ballard

class TokenizerIdentifierTests: XCTestCase {

	func testSimpleIdentifier1() {
		let s = "🐯"
		let tokens = try! Tokenizer.tokens(for: s)
		XCTAssertEqual(tokens.count, 1)
		let token = tokens.first!
		compareIdentifierToken(token, "🐯", TextPosition(0, 0))
	}

	func testSimpleIdentifier2() {
		let s = "table"
		let tokens = try! Tokenizer.tokens(for: s)
		XCTAssertEqual(tokens.count, 1)
		let token = tokens.first!
		compareIdentifierToken(token, "table", TextPosition(0, 0))
	}

	func testSimpleIdentifier3() {
		let s = "_foo"
		let tokens = try! Tokenizer.tokens(for: s)
		XCTAssertEqual(tokens.count, 1)
		let token = tokens.first!
		compareIdentifierToken(token, "_foo", TextPosition(0, 0))
	}

	func testSimpleIdentifier4() {
		let s = "_f🐯oo🐯"
		let tokens = try! Tokenizer.tokens(for: s)
		XCTAssertEqual(tokens.count, 1)
		let token = tokens.first!
		compareIdentifierToken(token, "_f🐯oo🐯", TextPosition(0, 0))
	}

	func testSimpleIdentifier5() {
		let s = "_"
		let tokens = try! Tokenizer.tokens(for: s)
		XCTAssertEqual(tokens.count, 1)
		let token = tokens.first!
		compareIdentifierToken(token, "_", TextPosition(0, 0))
	}

	func testIdentifierPerformance() {
		let s = "_f🐯oo🐯"
		self.measure {
			let _ = try! Tokenizer.tokens(for: s)
		}
	}
}

private extension TokenizerIdentifierTests {

	func compareIdentifierToken(_ token: Token, _ expectedValue: String, _ expectedTextPosition: TextPosition) {
		XCTAssertEqual(token.type, TokenType.identifier)
		XCTAssertEqual((token.value! as! ValueString).string, expectedValue)
		XCTAssertEqual(token.textPosition, expectedTextPosition)
	}
}
