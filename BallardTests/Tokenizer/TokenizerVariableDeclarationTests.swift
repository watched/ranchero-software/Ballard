//
//  TokenizerVariableDeclarationTests.swift
//  BallardTests
//
//  Created by Brent Simmons on 11/20/18.
//  Copyright © 2018 Ranchero Software. All rights reserved.
//

import XCTest
@testable import Ballard

class TokenizerVariableDeclarationTests: XCTestCase {

	func testSimpleLetDeclaration() {
		let s = "let x = 8"
		let tokens = try! Tokenizer.tokens(for: s)
		XCTAssertEqual(tokens.count, 4)

		XCTAssertEqual(tokens[0].type, .letToken)
		XCTAssertEqual(tokens[1].type, .identifier)
		XCTAssertEqual(tokens[2].type, .assign)
		XCTAssertEqual(tokens[3].type, .literalInt)

		XCTAssertEqual(tokens[3].textPosition.characterIndex, 8)
		XCTAssertEqual(tokens[3].value! as! Int, 8)
	}
}
