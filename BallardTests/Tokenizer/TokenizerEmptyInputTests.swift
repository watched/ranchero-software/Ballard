//
//  TokenizerEmptyInputTests.swift
//  BallardTests
//
//  Created by Ole Begemann on 18/12/09.
//  Copyright © 2018 Ranchero Software. All rights reserved.
//

import XCTest
@testable import Ballard

class TokenizerEmptyInputTests: XCTestCase {

    func testEmptyInput() {
        let s = ""
        let tokens = try! Tokenizer.tokens(for: s)
        XCTAssertTrue(tokens.isEmpty)
    }
}
