//
//  TokenizerStringTests.swift
//  BallardTests
//
//  Created by Brent Simmons on 11/19/18.
//  Copyright © 2018 Ranchero Software. All rights reserved.
//

import XCTest
@testable import Ballard

class TokenizerStringTests: XCTestCase {

	func testSimpleString1() {
		let s = "\"Hello, world!\""
		let tokens = try! Tokenizer.tokens(for: s)
		XCTAssertEqual(tokens.count, 1)
		let token = tokens.first!
		compareStringToken(token, "Hello, world!", TextPosition(0, 0))
	}

	func testStringPerformance() {
		let s = "\"Hello, world!\""
		self.measure {
			let _ = try! Tokenizer.tokens(for: s)
		}
	}
}

private extension TokenizerStringTests {

	func compareStringToken(_ token: Token, _ expectedValue: String, _ expectedTextPosition: TextPosition) {
		XCTAssertEqual(token.type, TokenType.literalString)
		XCTAssertEqual((token.value! as! ValueString).string, expectedValue)
		XCTAssertEqual(token.textPosition, expectedTextPosition)
	}
}
