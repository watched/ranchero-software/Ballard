//
//  TokenizerTwoLineScriptTests.swift
//  BallardTests
//
//  Created by Brent Simmons on 11/20/18.
//  Copyright © 2018 Ranchero Software. All rights reserved.
//

import XCTest
@testable import Ballard

class TokenizerTwoLineScriptTests: XCTestCase {

	func testVariableDeclarationAndAssignment() {
		let s = "var x = 8\nx = 10"
		let tokens = try! Tokenizer.tokens(for: s)
		XCTAssertEqual(tokens.count, 7)

		XCTAssertEqual(tokens[0].type, .varToken)
		XCTAssertEqual(tokens[1].type, .identifier)
		XCTAssertEqual(tokens[2].type, .assign)
		XCTAssertEqual(tokens[3].type, .literalInt)

		XCTAssertEqual(tokens[3].textPosition.characterIndex, 8)
		XCTAssertEqual(tokens[3].value! as! Int, 8)
	}

    func testCRLFLineEndings() {
        let s = "1\r\n2"
        let tokens = try! Tokenizer.tokens(for: s)
        XCTAssertEqual(tokens.count, 2)

        XCTAssertEqual(tokens[0].type, .literalInt)
        XCTAssertEqual(tokens[1].type, .literalInt)

        XCTAssertEqual(tokens[0].textPosition.characterIndex, 0)
        XCTAssertEqual(tokens[0].value! as! Int, 1)
        XCTAssertEqual(tokens[1].textPosition.characterIndex, 1)
        XCTAssertEqual(tokens[1].value! as! Int, 2)
    }
}
