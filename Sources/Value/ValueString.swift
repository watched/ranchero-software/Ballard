//
//  ValueString.swift
//  FrontierData
//
//  Created by Brent Simmons on 4/21/17.
//  Copyright © 2017 Ranchero Software. All rights reserved.
//

import Foundation

public struct ValueString: Value {

	let string: String
	let applicationType: String?

	public var valueType: ValueType {
		return .string
	}

	init(_ s: String, applicationType: String? = nil) {
		self.string = s
		self.applicationType = applicationType
	}

	public func asString() throws -> ValueString {
		return self
	}
}
