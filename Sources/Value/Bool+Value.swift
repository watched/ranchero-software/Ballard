//
//  ValueBool.swift
//  FrontierData
//
//  Created by Brent Simmons on 4/21/17.
//  Copyright © 2017 Ranchero Software. All rights reserved.
//

import Foundation

extension Bool: Value {

	public var valueType: ValueType {
		return .boolean
	}
	
	public func asBool() throws -> Bool {
		return self
	}
	
	public func asInt() throws -> Int {
		return forcedIntValue()
	}
	
	public func asDouble() throws -> Double {
		return Double(forcedIntValue())
	}
	
	public func asDate() throws -> Date {
		return Date(timeIntervalSince1970: forcedDoubleValue())
	}

	private static let trueString = ValueString("true")
	private static let falseString = ValueString("false")

	public func asString() throws -> ValueString {
		return self ? Bool.trueString : Bool.falseString
	}
	
	public func asArray() throws -> [Any] {
		return [self as Any]
	}
}

private extension Bool {

	func forcedIntValue() -> Int {
		return self ? 1 : 0
	}

	func forcedDoubleValue() -> Double {
		return Double(forcedIntValue())
	}
}
