//
//  Value.swift
//  Ballard
//
//  Created by Brent Simmons on 4/20/17.
//  Copyright © 2017 Ranchero Software. All rights reserved.
//

import Foundation

// Protocol for value types: boolean, int, double, date, string, binary data, address, address.
//
// Go with asBool etc. because boolValue and similar conflict with existing methods.

public protocol Value {
	
	var valueType: ValueType { get }

	func asBool() throws -> Bool
	func asInt() throws -> Int
	func asDouble() throws -> Double
	func asDate() throws -> Date
	func asBinary() throws -> Binary
	func asAddress() throws -> Address
	func asString() throws -> ValueString
	func asArray() throws -> [Any]

	// Operations
	
	func negativeValue() throws -> Value
	func not() throws -> Bool
	func beginsWith(_ otherValue: Value) throws -> Bool
	func endsWith(_ otherValue: Value) throws -> Bool
	func contains(_ otherValue: Value) throws -> Bool
	func mod(_ otherValue: Value) throws -> Value
	func add(_ otherValue: Value) throws -> Value
	func subtract(_ otherValue: Value) throws -> Value
	func divide(_ otherValue: Value) throws -> Value
	func multiply(_ otherValue: Value) throws -> Value
	
	// Comparisons
	
	func compareTo(_ otherValue: Value) throws -> ComparisonResult
	// If you implement compareTo, you can probably stick with the default version of these.
	func equals(_ otherValue: Value) throws -> Bool
	func lessThan(_ otherValue: Value) throws -> Bool
	func lessThanEqual(_ otherValue: Value) throws -> Bool
	func greaterThan(_ otherValue: Value) throws -> Bool
	func greaterThanEqual(_ otherValue: Value) throws -> Bool
}

// MARK: - Coercions

public extension Value {

	func asBool() throws -> Bool {
		throw RuntimeError(.coercionNotPossible)
	}

	func asInt() throws -> Int {
		throw RuntimeError(.coercionNotPossible)
	}

	func asDouble() throws -> Double {
		throw RuntimeError(.coercionNotPossible)
	}

	func asDate() throws -> Date {
		throw RuntimeError(.coercionNotPossible)
	}

	func asBinary() throws -> Binary {
		throw RuntimeError(.coercionNotPossible)
	}

	func asAddress() throws -> Address {
		throw RuntimeError(.coercionNotPossible)
	}

	func asString() throws -> ValueString {
		throw RuntimeError(.coercionNotPossible)
	}
	
	func asArray() throws -> [Any] {
		throw RuntimeError(.coercionNotPossible)
	}

	func compareTo(_ otherValue: Value) throws -> ComparisonResult {
		throw RuntimeError(.comparisonNotPossible)
	}
	
	func equals(_ otherValue: Value) throws -> Bool {
		
		do {
			let comparisonResult = try compareTo(otherValue)
			return comparisonResult == .orderedSame
		}
		catch { throw error }
	}
	
	func negativeValue() throws -> Value {
		
		throw RuntimeError(.negativeValueNotPossible)
	}
	
	func not() throws -> Bool {
		
		throw RuntimeError(.notValueNotPossible)
	}
	
	func beginsWith(_ otherValue: Value) throws -> Bool {
		
		return false // TODO
	}
	
	func endsWith(_ otherValue: Value) throws -> Bool {
		
		return false // TODO
	}
	
	func contains(_ otherValue: Value) throws -> Bool {
		
		return false // TODO
	}
	
	func lessThan(_ otherValue: Value) throws -> Bool {
		
		do {
			let comparisonResult = try compareTo(otherValue)
			return comparisonResult == .orderedAscending
		}
		catch { throw error }
	}
	
	func lessThanEqual(_ otherValue: Value) throws -> Bool {
		
		do {
			let comparisonResult = try compareTo(otherValue)
			return comparisonResult == .orderedAscending || comparisonResult == .orderedSame
		}
		catch { throw error }
	}
	
	func greaterThan(_ otherValue: Value) throws -> Bool {
		
		do {
			let comparisonResult = try compareTo(otherValue)
			return comparisonResult == .orderedDescending
		}
		catch { throw error }
	}
	
	func greaterThanEqual(_ otherValue: Value) throws -> Bool {
		
		do {
			let comparisonResult = try compareTo(otherValue)
			return comparisonResult == .orderedDescending || comparisonResult == .orderedSame
		}
		catch { throw error }
	}
	
	func mod(_ otherValue: Value) throws -> Value {
		
		return 0 // TODO
	}
	
	func add(_ otherValue: Value) throws -> Value {
		
		return 0 // TODO
	}
	
	func subtract(_ otherValue: Value) throws -> Value {
		
		return 0 // TODO
	}
	
	func divide(_ otherValue: Value) throws -> Value {
		
		return 0 // TODO
	}
	
	func multiply(_ otherValue: Value) throws -> Value {
		
		return 0 // TODO
	}
}

