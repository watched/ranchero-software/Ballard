//
//  ValueBinary.swift
//  FrontierData
//
//  Created by Brent Simmons on 4/22/17.
//  Copyright © 2017 Ranchero Software. All rights reserved.
//

import Foundation

public struct Binary: Value {

	let data: Data

	public var valueType: ValueType {
		get {
			return .binary
		}
	}
}
