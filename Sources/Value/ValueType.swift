//
//  ValueType.swift
//  Ballard
//
//  Created by Brent Simmons on 4/20/17.
//  Copyright © 2017 Ranchero Software. All rights reserved.
//

import Foundation

public enum ValueType: Int {
	case none = 0 // nil
	case boolean = 1
	case int = 2
	case double = 3
	case string = 4
	case date = 5
	case binary = 6
	case address = 7
	case array = 8
}
