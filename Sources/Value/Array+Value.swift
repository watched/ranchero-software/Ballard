//
//  Array+Value.swift
//  Ballard
//
//  Created by Brent Simmons on 11/17/18.
//  Copyright © 2018 Ranchero Software. All rights reserved.
//

import Foundation

extension Array: Value {

	public var valueType: ValueType {
		return .array
	}

	public func asBool() throws -> Bool {
		return !isEmpty
	}

	public func asArray() throws -> [Any] {
		return self
	}
}
