//
//  ValueInt.swift
//  FrontierData
//
//  Created by Brent Simmons on 4/21/17.
//  Copyright © 2017 Ranchero Software. All rights reserved.
//

import Foundation

extension Int: Value {
	
	public var valueType: ValueType {
		get {
			return .int
		}
	}
	
	public func asBool() throws -> Bool {		
		return self != 0
	}
	
	public func asInt() throws -> Int {
		return self
	}
	
	public func asDouble() throws -> Double {
		return Double(self)
	}
	
	public func asDate() throws -> Date {
		return Date(timeIntervalSince1970: Double(self))
	}
	
	public func asString() throws -> ValueString {
		return ValueString("\(self)")
	}
	
	public func asArray() throws -> [Any] {
		return [self as Any]
	}
	
	public func negativeValue() throws -> Value {
		return 0 - self
	}
}
