//
//  ValueDouble.swift
//  FrontierData
//
//  Created by Brent Simmons on 4/22/17.
//  Copyright © 2017 Ranchero Software. All rights reserved.
//

import Foundation

extension Double: Value {
	
	public var valueType: ValueType {
		return .double
	}
	
	public func asBool() throws -> Bool {
		return forcedBoolValue()
	}
	
	public func asInt() throws -> Int {
		return forcedIntValue()
	}
	
	public func asDouble() throws -> Double {
		return self
	}
	
	public func asDate() throws -> Date {
		return Date(timeIntervalSince1970: self)
	}
	
	public func asString() throws -> ValueString {
		return ValueString("\(self)")
	}
	
	public func asArray() throws -> [Any] {
		return [self as Any]
	}

	public func negativeValue() throws -> Value {
		return 0 - self
	}

	public func not() throws -> Bool {
		return !forcedBoolValue()
	}
}

private extension Double {

	func forcedBoolValue() -> Bool {
		return forcedIntValue() != 0
	}

	func forcedIntValue() -> Int {
		return Int(self)
	}
}
