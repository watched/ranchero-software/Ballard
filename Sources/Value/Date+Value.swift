//
//  ValueDate.swift
//  FrontierData
//
//  Created by Brent Simmons on 4/21/17.
//  Copyright © 2017 Ranchero Software. All rights reserved.
//

import Foundation

extension Date: Value {
	
	public var valueType: ValueType {
		return .date
	}

	public func asBool() throws -> Bool {
		return forcedIntValue() != 0
	}

	public func asInt() throws -> Int {
		return forcedIntValue()
	}

	public func asDouble() throws -> Double {		
		return timeIntervalSince1970
	}

	public func asDate() throws -> Date {
		return self
	}

	public func asString() throws -> ValueString {
		return ValueString("\(self)")
	}

	public func asArray() throws -> [Any] {
		return [self as Any]
	}
}

private extension Date {

	func forcedIntValue() -> Int {
		return Int(timeIntervalSince1970)
	}
}
