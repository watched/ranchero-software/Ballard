//
//  ValueNil.swift
//  Ballard
//
//  Created by Brent Simmons on 11/14/18.
//  Copyright © 2018 Ranchero Software. All rights reserved.
//

import Foundation

extension NSNull: Value {

	public var valueType: ValueType {
		return .none
	}

	public func asBool() throws -> Bool {
		return false
	}

	public func asInt() throws -> Int {
		return 0
	}

	public func asDouble() throws -> Double {
		return 0.0
	}

	public func asDate() throws -> Date {
		return Date(timeIntervalSince1970: 0.0)
	}

	public func asArray() throws -> [Any] {
		return [Any]()
	}
}
