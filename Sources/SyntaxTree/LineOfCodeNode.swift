//
//  LineOfCodeNode.swift
//  Ballard
//
//  Created by Brent Simmons on 5/11/19.
//  Copyright © 2019 Ranchero Software. All rights reserved.
//

import Foundation

// This is used for syntax highlighting. It’s a fatar error to evaluate it.

struct LineOfCodeNode: SyntaxTreeNode {

	let operation: SyntaxTreeOperation = .lineOfCode
	let textPosition: TextPosition
	let statementList: StatementListNode

	init(statementList: StatementListNode, textPosition: TextPosition) {
		self.statementList = statementList
		self.textPosition = textPosition
	}

	func evaluate(_ stack: Stack) throws -> EvaluationResult {
		preconditionFailure("evaluate called on a LineOfCodeNode.")
	}
}
