//
//  ContinueNode.swift
//  Ballard
//
//  Created by Brent Simmons on 5/2/19.
//  Copyright © 2019 Ranchero Software. All rights reserved.
//

import Foundation

struct ContinueNode: SyntaxTreeNode {

	let operation: SyntaxTreeOperation = .continueOp
	let textPosition: TextPosition

	init(textPosition: TextPosition) {
		self.textPosition = textPosition
	}

	func evaluate(_ stack: Stack) throws -> EvaluationResult {
		return .continueResult
	}
}
