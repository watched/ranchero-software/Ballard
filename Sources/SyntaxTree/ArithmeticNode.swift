//
//  ArithmeticNode.swift
//  Ballard
//
//  Created by Brent Simmons on 4/27/19.
//  Copyright © 2019 Ranchero Software. All rights reserved.
//

import Foundation

struct ArithmeticNode: SyntaxTreeNode {

	let operation: SyntaxTreeOperation
	let textPosition: TextPosition
	let leftNode: SyntaxTreeNode
	let rightNode: SyntaxTreeNode

	init(operation: SyntaxTreeOperation, leftNode: SyntaxTreeNode, rightNode: SyntaxTreeNode, textPosition: TextPosition) {
		precondition(operation == .add || operation == .subtract || operation == .multiply || operation == .divide || operation == .mod)
		self.operation = operation
		self.leftNode = leftNode
		self.rightNode = rightNode
		self.textPosition = textPosition
	}

	func evaluate(_ stack: Stack) throws -> EvaluationResult {
		let leftValue = try value(from: leftNode, stack)
		let rightValue = try value(from: rightNode, stack)

		let value: Value
		switch operation {
		case .add:
			value = try leftValue.add(rightValue)
		case .subtract:
			value = try leftValue.subtract(rightValue)
		case .multiply:
			value = try leftValue.multiply(rightValue)
		case .divide:
			value = try leftValue.divide(rightValue)
		case .mod:
			value = try leftValue.mod(rightValue)
		default:
			throw RuntimeError(.unexpectedOperation)
		}

		return .valueResult(value)
	}
}
