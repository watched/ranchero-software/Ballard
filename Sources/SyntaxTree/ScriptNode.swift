//
//  ScriptNode.swift
//  Ballard
//
//  Created by Brent Simmons on 5/4/17.
//  Copyright © 2017 Ranchero Software. All rights reserved.
//

import Foundation

// Top level of every script.

struct ScriptNode: SyntaxTreeNode {

	let operation: SyntaxTreeOperation = .script
	let textPosition = TextPosition.start() // By definition
	let statementListNode: StatementListNode

	init(_ statementListNode: StatementListNode) {
		self.statementListNode = statementListNode
	}
	
	func evaluate(_ stack: Stack) throws -> EvaluationResult {
		return try evaluateStatementList(statementListNode, stack)
	}
}
