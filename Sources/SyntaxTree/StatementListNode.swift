//
//  StatementListNode.swift
//  Ballard
//
//  Created by Brent Simmons on 4/27/19.
//  Copyright © 2019 Ranchero Software. All rights reserved.
//

import Foundation

struct StatementListNode: SyntaxTreeNode {

	let operation: SyntaxTreeOperation = .statementList
	let textPosition: TextPosition
	let statements: [SyntaxTreeNode]

	init(_ statements: [SyntaxTreeNode], _ textPosition: TextPosition) {
		self.statements = statements
		self.textPosition = textPosition
	}

	func evaluate(_ stack: Stack) throws -> EvaluationResult {
		return try evaluateNodes(statements, stack)
	}
}
