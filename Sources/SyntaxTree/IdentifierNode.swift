//
//  IdentifierNode.swift
//  Ballard
//
//  Created by Brent Simmons on 4/27/19.
//  Copyright © 2019 Ranchero Software. All rights reserved.
//

import Foundation

struct IdentifierNode: SyntaxTreeNode {

	let operation: SyntaxTreeOperation = .value
	let textPosition: TextPosition
	let name: String

	init(name: String, textPosition: TextPosition) {
		self.name = name
		self.textPosition = textPosition
	}

	func evaluate(_ stack: Stack) throws -> EvaluationResult {
		return .valueResult(ValueString(name))
	}
}
