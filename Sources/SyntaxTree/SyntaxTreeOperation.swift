//
//  SyntaxTreeOperation.swift
//  Ballard
//
//  Created by Brent Simmons on 4/20/17.
//  Copyright © 2017 Ranchero Software. All rights reserved.
//

import Foundation

// These have values so that code trees can be serialized/deserialized.
//
// Where names would conflict with Swift keywords, the suffix “Op” has been added.

public enum SyntaxTreeOperation: Int {

	case script = 1
	case lineOfCode = 2 // Used only with parsing for syntax highlighting
	case statementList = 3
//	case value = 4
//	
//	// Arithmetic
//	case add = 32
//	case subtract = 33
//	case multiply = 34
//	case divide = 35
//	case mod = 36 // %
//
//	// Comparison
//	case ifOp = 64
//	case equals = 65
//	case notEquals = 66
//	case greaterThan = 67
//	case lessThan = 68
//	case greaterThanEquals = 69
//	case lessThanEquals = 70
//	case orOr = 71
//	case andAnd = 72
//
//	// Variables
//	case varDeclaration = 128
//	case letDeclaration = 129
//	case identifier = 130
//	case assign = 131
//
//	// Control flow
//	case breakOp = 160
//	case returnOp = 161
//	case continueOp = 162
//	case block = 163
//	case switchOp = 164
//	case caseOp = 165
//	case caseBody = 166
//	case tryOp = 167
//	case function = 168
//
//	// Loops
//	case whileLoop = 193
//	case forInLoop = 194
//
//	// Unary - prefix
//	case negative = 256 // -
//	case not = 257 // !
//
//	// Unary - postfix
//	case increment = 288 // ++
//	case decrement = 289 // --
//	case dereference = 290 // ^
}

