//
//  ValueNode.swift
//  Ballard
//
//  Created by Brent Simmons on 4/27/19.
//  Copyright © 2019 Ranchero Software. All rights reserved.
//

import Foundation

struct ValueNode: SyntaxTreeNode {

	let operation: SyntaxTreeOperation = .value
	let textPosition: TextPosition
	let value: Value

	init(value: Value, textPosition: TextPosition) {
		self.value = value
		self.textPosition = textPosition
	}

	func evaluate(_ stack: Stack) throws -> EvaluationResult {
		return .valueResult(value)
	}
}
