//
//  Token.swift
//  Ballard
//
//  Created by Brent Simmons on 11/12/18.
//  Copyright © 2018 Ranchero Software. All rights reserved.
//

import Foundation

// When name conflicts with a Swift keyword, the suffix "Token" is added.

struct Token {
	let type: TokenType
	let value: Value?
	let textPosition: TextPosition
}

enum TokenType: Int, Equatable {

	case empty

	case not
	case and
	case or

	case equals
	case notEquals
	case greaterThan
	case lessThan
	case greaterThanEquals
	case lessThanEquals

	case forToken
	case inToken
	case whileToken

	case ifToken
	case elseToken
	case tryToken

	case block

	case breakToken
	case continueToken
	case returnToken

	case varToken
	case letToken

	case def

	case trueToken
	case falseToken

	case literalString
	case literalDouble
	case literalInt
	case identifier

	case assign

	case plus
	case minus
	case star
	case slash
	case percent

	case plusPlus
	case minusMinus

	case comma

	case leftParen
	case rightParen
	case semicolon
	case leftBrace
	case rightBrace
	case dot
	case colon
	case leftBracket
	case rightBracket

	case at // @ (address)
	case caret //^ (dereference)

	case comment

}
