//
//  Tokenizer.swift
//  Ballard
//
//  Created by Brent Simmons on 4/29/17.
//  Copyright ©2017 Ranchero Software. All rights reserved.
//

import Foundation

typealias TokenizerResult = Result<[Token], TokenizerError>

public class Tokenizer {

	private let codeString: String
	private let endIndex: String.Index
	private var currentIndex: String.Index
	private var position: TextPosition = TextPosition.start()
	private var tokens = [Token]()

	static func tokens(for codeString: String) -> TokenizerResult {
		let tokenizer = Tokenizer(codeString: codeString)

		do {
			try tokenizer.parseTokens()
			return .success(tokenizer.tokens)
		} catch let error as TokenizerError {
			return .failure(error)
		} catch {
			preconditionFailure("Unknown fatal error in Tokenizer.")
		}
 	}

	private init(codeString: String) {
		self.codeString = codeString
		self.currentIndex = codeString.startIndex
		self.endIndex = codeString.endIndex
	}
}

// MARK: - Private

private extension Tokenizer {

	func parseTokens() throws {
        while let oneToken = try popNextToken() {
            tokens.append(oneToken)
        }
	}

	func popNextToken() throws -> Token? {
		if atEndOfString() || !skipBlanksAndComments() {
			return nil
		}
		guard let ch = currentCharacter() else {
			return nil
		}

		let savedPosition = position

		func token(_ type: TokenType, _ value: Value? = nil) -> Token {
			return Token(type: type, value: value, textPosition: savedPosition)
		}

        if ch == singleQuote || ch == doubleQuote {
            let s = try popStringLiteral()
            let value = ValueString(s)
            return token(.literalString, value)
        }
        else if ch.isDigit() {
            if let n = popNumberLiteral() {
                let tokenType: TokenType = n is Double ? .literalDouble : .literalInt
                return token(tokenType, n)
            }
            throw TokenizerError(.unableToTokenize)
        }
        else if ch.isLanguagePunctuation() {
            if let tokenType = popLanguagePunctuationToken() {
                return token(tokenType)
            }
            throw TokenizerError(.unableToTokenize)
        }
        else if ch.isLegalFirstIdentifierCharacter() {
            let identifier = popIdentifier()
            if let keywordTokenType = Keywords.token(for: identifier) {
                return token(keywordTokenType)
            }
            else {
                return token(.identifier, ValueString(identifier))
            }
        }

		throw TokenizerError(.unableToTokenize)
	}

	// MARK: - Punctuation Tokens

	func popLanguagePunctuationToken() -> TokenType? {
		guard let ch = popCharacter() else {
			return nil
		}
		let characterTwo = currentCharacter()
		return tokenType(for: ch, characterTwo: characterTwo)
	}

	func tokenType(for ch: Character, characterTwo: Character?) -> TokenType? {

		if let characterTwo = characterTwo {

			switch ch {

			case "&":
				if characterTwo == "&" {
					moveAheadCharacter()
					return .and
				}
				return nil
			case "|":
				if characterTwo == "|" {
					moveAheadCharacter()
					return .or
				}
				return nil
			case "=":
				if characterTwo == "=" {
					moveAheadCharacter()
					return .equals
				}
			case "!":
				if characterTwo == "=" {
					moveAheadCharacter()
					return .notEquals
				}
			case ">":
				if characterTwo == "=" {
					moveAheadCharacter()
					return .greaterThanEquals
				}
			case "<":
				if characterTwo == "=" {
					moveAheadCharacter()
					return .lessThanEquals
				}
			case "+":
				if characterTwo == "+" {
					moveAheadCharacter()
					return .plusPlus
				}
			case "-":
				if characterTwo == "-" {
					moveAheadCharacter()
					return .minusMinus
				}
			case "/":
				if characterTwo == "/" {
					moveAheadCharacter()
					return .comment
				}
			default:
				break
			}
		}

		switch(ch) {

		case "=":
			return .assign
		case "!":
			return .not
		case ">":
			return .greaterThan
		case "<":
			return .lessThan
		case "+":
			return .plus
		case "-":
			return .minus
		case "/":
			return .slash
		case "*":
			return .star
		case "%":
			return .percent
		case "@":
			return .at
		case "^":
			return .caret
		case ".":
			return .dot
		case ",":
			return .comma
		case ":":
			return .colon
		case ";":
			return .semicolon
		case "(":
			return .leftParen
		case ")":
			return .rightParen
		case "{":
			return .leftBrace
		case "}":
			return .rightBrace
		case "[":
			return .leftBracket
		case "]":
			return .rightBracket
		default:
			return nil
		}
	}

	// MARK: - Identifiers

	func popIdentifier() -> String {
		var s = ""

		while true {
			guard let ch = currentCharacter() else {
				return s
			}
			if !ch.isLegalIdentifierCharacter() {
				return s
			}

			s.append(ch)
			if !moveAheadCharacter() {
				return s
			}
		}
	}

	// MARK: - Literals

	func popNumberLiteral() -> Value? {
		var isFloat = false
		var isHex = false
		var numberString = ""
		guard let ch = currentCharacter() else {
			return nil
		}

		if ch == "0" {
			moveAheadCharacter()
			guard let ch = currentCharacter() else {
				return 0
			}
			if ch == "x" {
				moveAheadCharacter()
				isHex = true
			}
		}

		func pullNumberValue() -> Value {
			if numberString.isEmpty {
				return 0
			}
			if isFloat {
				return (numberString as NSString).doubleValue
			}
			else if isHex {
				if let n = numberString.hexStringToNumber() {
					return n
				}
				return 0
			}
			if let n = numberString.stringToNumber() {
				return n
			}
			return 0
			}

		while true {

			guard let ch = currentCharacter() else {
				return pullNumberValue()
			}

			if ch == "." && !isHex && !isFloat {
				isFloat = true
			}
			else {
				if !ch.isDigit() && !(isHex && (isxdigit(ch.asInt32()) != 0)) {
					return pullNumberValue()
				}
			}

			numberString.append(ch)
			moveAheadCharacter()
		}
	}

	func popStringLiteral() throws -> String {
		let savedPosition = position

		func stringNotTerminatedError() -> TokenizerError {
			return TokenizerError(.stringNotTerminated, textPosition: savedPosition)
		}

		guard let quoteCharacter = currentCharacter() else {
			throw stringNotTerminatedError()
		}
		if !moveAheadCharacter() {
			throw stringNotTerminatedError()
		}

		var s = ""

		while true {

			guard let ch = popCharacter() else {
				throw stringNotTerminatedError()
			}
			if ch == quoteCharacter {
				break
			}
			if ch.isReturnOrLineFeedOrBoth() {
				throw stringNotTerminatedError()
			}

			if ch == backslash {
				if let unescapedCharacter = parseEscapeSequence() {
					s.append(unescapedCharacter)
				}
				else {
					throw stringNotTerminatedError()
				}
			}
			else {
				s.append(ch)
			}
		}

		return s
	}

	func parseEscapeSequence() -> Character? {
		guard let ch = popCharacter() else {
			return nil
		}

		switch ch {

		case "n":
			return lineFeed
		case "r":
			return carriageReturn
		case "t":
			return tab
		case backslash:
			return backslash
		case singleQuote:
			return singleQuote
		case doubleQuote:
			return doubleQuote

		default:
			return ch
		}
	}

	// MARK: - Cursor Position

	func atEndOfString() -> Bool {
		return currentIndex == endIndex
	}

	@discardableResult
	func moveAheadCharacter() -> Bool {
		if atEndOfString() {
			return false
		}
		currentIndex = codeString.index(after: currentIndex)
		position = position.nextCharacter()
		guard let ch = currentCharacter() else {
			return false
		}
		if ch.isReturnOrLineFeedOrBoth() {
			position = position.nextLine()
		}
		return true
	}

	func currentCharacter() -> Character? {
		if atEndOfString() {
			return nil
		}
		return codeString[currentIndex]
	}

	func popCharacter() -> Character? {
		guard let ch = currentCharacter() else {
			return nil
		}
		moveAheadCharacter()
		return ch
	}

	func nextCharacter() -> Character? {
		if atEndOfString() {
			return nil
		}
		let ix = codeString.index(after: currentIndex)
		if ix == endIndex {
			return nil
		}
		return codeString[ix]
	}

	func skipBlanksAndComments() -> Bool {
		// Return false if at end of string.
		while true {
			guard let ch = currentCharacter() else {
				return false
			}

			var isStartComment = false
			if ch == forwardSlash {
				if let chNext = nextCharacter(), chNext == forwardSlash {
					isStartComment = true
				}
			}

			if isStartComment {
				skipComment()
			}
			else {
				if !ch.isWhiteSpace() {
					return true
				}
				moveAheadCharacter()
			}
		}
	}

	func skipComment() {
		// Comments go all the way to the end of the line.
		while true {
			guard let ch = popCharacter() else {
				return
			}
			if ch.isReturnOrLineFeedOrBoth() {
				return
			}
		}
	}
}

// MARK: - Character Extension

private let digitSet: Set<Character> = Set(["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"])

// Punctuation recognized by the language.
private let languagePunctuationSet: Set<Character> = Set(["&", "|", "=", "!", ">", "<", "+", "-", "/",
												  "*", "%", "@", "^", ".", ",", ":", ";", "(",
												  ")", "{", "}", "[", "]"])

private let lineFeed: Character = "\n"
private let carriageReturn: Character = "\r"
private let carriageReturnLineFeed: Character = "\r\n"
private let singleQuote: Character = "'"
private let doubleQuote: Character = "\""
private let forwardSlash: Character = "/"
private let space: Character = " "
private let tab: Character = "\t"
private let backslash: Character = "\\"

private extension Character {

	func isReturnOrLineFeedOrBoth() -> Bool {
		return self == lineFeed || self == carriageReturn || self == carriageReturnLineFeed
	}

	func isWhiteSpace() -> Bool {
		return self == tab || self == space || isReturnOrLineFeedOrBoth()
	}

	func asInt32() -> Int32 {
		let s = String(self)
		return Int32(s.utf8[s.utf8.startIndex])
	}

	func isDigit() -> Bool {
		return digitSet.contains(self)
	}

	func isLanguagePunctuation() -> Bool {
		return languagePunctuationSet.contains(self)
	}

	func isLegalFirstIdentifierCharacter() -> Bool {
		if isDigit() || isReturnOrLineFeedOrBoth() || isWhiteSpace() || isLanguagePunctuation() {
			return false
		}
		return true
	}

	func isLegalIdentifierCharacter() -> Bool {
		if isReturnOrLineFeedOrBoth() || isWhiteSpace() || isLanguagePunctuation() {
			return false
		}
		return true
	}
}

// MARK: - String Extension

private extension String {

	func hexStringToNumber() -> Int? {
		let scanner = Scanner(string: self)
		var n: UInt64 = 0
		if scanner.scanHexInt64(&n) {
			return Int(n)
		}
		return nil
	}

	func stringToNumber() -> Int? {
		let scanner = Scanner(string: self)
		var n = 0
		if scanner.scanInt(&n) {
			return n
		}
		return nil
	}
}

