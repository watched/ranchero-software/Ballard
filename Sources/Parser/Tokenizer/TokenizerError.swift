//
//  TokenizerError.swift
//  Ballard
//
//  Created by Brent Simmons on 11/12/18.
//  Copyright © 2018 Ranchero Software. All rights reserved.
//

import Foundation

public struct TokenizerError: ErrorWithTextPosition {

	public enum TokenizerErrorType {
		case illegalToken
		case unableToTokenize
		case stringNotTerminated
	}

	public let errorType: TokenizerErrorType
	public let textPosition: TextPosition?

	public init(_ errorType: TokenizerErrorType, textPosition: TextPosition? = nil) {
		self.errorType = errorType
		self.textPosition = textPosition
	}
}

