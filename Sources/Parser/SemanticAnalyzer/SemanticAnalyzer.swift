//
//  TokenizerError.swift
//  Ballard
//
//  Created by Brent Simmons on 05/10/19.
//  Copyright © 2018 Ranchero Software. All rights reserved.
//

// After the parser has done its job, the semantic analyzer
// goes through and looks for possible illegal or dubious things
// that the parser wouldn’t have caught.
//
// Example: a break or continue statement used outside of a loop is an error.
// Another example: possible unwanted assignment in an if expression, as in `if x = 7`
//
// It returns errors and warnings based on what it finds.

struct SemanticAnalyzer {

	private let topNode: MutableSyntaxTreeNode

	private init(node: MutableSyntaxTreeNode) {
		self.topNode = node
	}

	static func analyze(_ node: MutableSyntaxTreeNode) -> ErrorsAndWarnings {
		let analyzer = SemanticAnalyzer(node: node)
		return analyzer.analyze()
	}
}

private extension SemanticAnalyzer {

	func analyze() -> ErrorsAndWarnings {
		// TODO
		return ErrorsAndWarnings(errors: nil, warnings: nil)
	}
}
