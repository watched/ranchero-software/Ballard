//
//  ParserWarning.swift
//  Ballard
//
//  Created by Brent Simmons on 5/4/19.
//  Copyright © 2019 Ranchero Software. All rights reserved.
//

import Foundation

public struct ParserWarning: ErrorWithTextPosition {

	public enum ParserWarningType {
		case placeholderWarning // TODO: fill out list of warnings
	}

	public let errorType: ParserWarningType
	public let textPosition: TextPosition?

	public init(_ errorType: ParserWarningType, textPosition: TextPosition? = nil) {
		self.errorType = errorType
		self.textPosition = textPosition
	}
}

