//
//  Concretizer.swift
//  Ballard
//
//  Created by Brent Simmons on 5/10/19.
//  Copyright © 2019 Ranchero Software. All rights reserved.
//

// Last step in creating as AST.
// Takes a tree of MutableSyntaxTreeNode and turns them into
// the appropriate immutable types.

import Foundation

struct Concretizer {

	static func concretize(_ node: MutableSyntaxTreeNode) -> SyntaxTreeNode {

		switch node.operation {
		case .script:
			return createScriptNode(node)
		case .lineOfCode:
			return createLineOfCodeNode(node)
		case .statementList:
			return createStatementListNode(node)
		}
	}
}

private extension Concretizer {

	static func createScriptNode(_ node: MutableSyntaxTreeNode) -> ScriptNode {
		guard let statements = node.statements else {
			
		}
		let statementListNode = createStatementListNode(with: node.statements!)
	}

	static func createLineOfCodeNode(_ node: MutableSyntaxTreeNode) -> LineOfCodeNode {

	}

	static func createStatementListNode(_ node: MutableSyntaxTreeNode) -> StatementListNode {

	}

	static func createStatementListNode(with statements: [SyntaxTreeNode]) -> StatementListNode {

	}

}
