//
//  MutableSyntaxTreeNode.swift
//  Ballard
//
//  Created by Brent Simmons on 5/4/19.
//  Copyright © 2019 Ranchero Software. All rights reserved.
//

import Foundation

// Used only by the parser during parsing.
// Not evaluatable, and never part of the tree the parser returns.
//
// Has all the attributes of the immutable nodes, combined into one.
// The semantic analysis pass of the parser creates immutable nodes based on these.

class MutableSyntaxTreeNode: SyntaxTreeNode {

	var operation: SyntaxTreeOperation
	var textPosition: TextPosition
	var statements: [SyntaxTreeNode]?
	var name: String?
	var value: Value?

	init(operation: SyntaxTreeOperation, textPosition: TextPosition) {
		self.operation = operation
		self.textPosition = textPosition
	}

	func addStatement(_ syntaxTreeNode: SyntaxTreeNode) {
		if statements == nil {
			statements = [SyntaxTreeNode]()
		}
		statements!.append(syntaxTreeNode)
	}

	func evaluate(_ stack: Stack) throws -> EvaluationResult {
		preconditionFailure("Can’t evaluate a MutableSyntaxTreeNode.")
	}
}
