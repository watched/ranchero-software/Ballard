//
//  ParserResult.swift
//  Ballard
//
//  Created by Brent Simmons on 5/4/19.
//  Copyright © 2019 Ranchero Software. All rights reserved.
//

import Foundation

public struct ParserResult {

	let firstNode: SyntaxTreeNode?
	let isForSyntaxHighlighting: Bool // Syntax highlighter parses a single line
	let errorsAndWarnings: ErrorsAndWarnings?

	init(node: SyntaxTreeNode?, isForSyntaxHighlighting: Bool, errorsAndWarnings: ErrorsAndWarnings?) {
		self.firstNode = node
		self.isForSyntaxHighlighting = isForSyntaxHighlighting
		self.errorsAndWarnings = errorsAndWarnings
	}

	func isEvaluatable() -> Bool {
		return !isForSyntaxHighlighting && firstNode != nil && !hasErrors()
	}

	func hasErrors() -> Bool {
		if let errors = errorsAndWarnings?.errors, !errors.isEmpty {
			return true
		}
		return false
	}

	static func resultWithTokenizerError(_ error: TokenizerError, forSyntaxHighlighting: Bool = false) -> ParserResult {
		let errorsAndWarnings = ErrorsAndWarnings(errors: [error], warnings: nil)
		return ParserResult(node: nil, isForSyntaxHighlighting: forSyntaxHighlighting, errorsAndWarnings: errorsAndWarnings)
	}
}

