//
//  ParserError.swift
//  Ballard
//
//  Created by Brent Simmons on 4/28/19.
//  Copyright © 2019 Ranchero Software. All rights reserved.
//

import Foundation

public struct ParserError: ErrorWithTextPosition {

	public enum ParserErrorType {
		case unexpectedTokenAtStartOfStatement
		case unexpectedToken
		case unexpectedEndOfTokens
		case unimplemented
		case breakNotAloneInStatement
		case continueNotAloneInStatement
	}

	public let errorType: ParserErrorType
	public let textPosition: TextPosition?

	public init(_ errorType: ParserErrorType, textPosition: TextPosition? = nil) {
		self.errorType = errorType
		self.textPosition = textPosition
	}
}

