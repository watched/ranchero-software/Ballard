//
//  Parser.swift
//  Ballard
//
//  Created by Brent Simmons on 4/27/19.
//  Copyright © 2019 Ranchero Software. All rights reserved.
//

import Foundation

class Parser {

	private let parsingForSyntaxHighlighting: Bool
	private let tokens: [Token]
	private let tokenCount: Int
	private let endIndex: Int
	private var currentIndex = 0
	private var errors = [ErrorWithTextPosition]()
	private var warnings = [ErrorWithTextPosition]()

	static func parseScriptText(_ scriptText: String, parsingForSyntaxHighlighting: Bool = false) -> ParserResult {
		let tokenizerResult = Tokenizer.tokens(for: scriptText)
		switch tokenizerResult {
		case .success(let tokens):
			let parser = Parser(tokens, parsingForSyntaxHighlighting: parsingForSyntaxHighlighting)
			return parser.parse()
		case .failure(let tokenizerError):
			return ParserResult.resultWithTokenizerError(tokenizerError)
		}
	}

	static func parseLineForSyntaxHighlighting(_ lineText: String) -> ParserResult {
		return parseScriptText(lineText, parsingForSyntaxHighlighting: true)
	}

	private init(_ tokens: [Token], parsingForSyntaxHighlighting: Bool) {
		precondition(!tokens.isEmpty)
		self.tokens = tokens
		self.tokenCount = tokens.count
		self.endIndex = tokens.count - 1
		self.parsingForSyntaxHighlighting = parsingForSyntaxHighlighting
	}
}

private extension Parser {

	func parse() -> ParserResult {
		let operation: SyntaxTreeOperation = parsingForSyntaxHighlighting ? .lineOfCode : .script
		let node = MutableSyntaxTreeNode(operation: operation, textPosition: TextPosition.start())

		do {
			try parseStatements(parentNode: node)
		} catch let error as ErrorWithTextPosition {
			errors = [error]
		} catch {
			preconditionFailure("Expected ErrorWithTextPosition, got other type of error.")
		}

		if !parsingForSyntaxHighlighting && errors.isEmpty {
			analyze(node)
		}

		// Concretize into ScriptNode etc. if:
		// 1) we’re not parsing for syntax highlighting, and
		// 2) there are zero errors.
		let hasErrors = !errors.isEmpty?
		let shouldConcretize = !hasErrors && !parsingForSyntaxHighlighting
		let resultNode = shouldConcretize ? Concretizer.concretize(node) : node
		let errorsAndWarnings = ErrorsAndWarnings(errors: errors, warnings: warnings)
		let parserResult = ParserResult(node: resultNode, isForSyntaxHighlighting: parsingForSyntaxHighlighting, errorsAndWarnings: errorsAndWarnings)
		return parserResult
	}

	func analyze(_ node: MutableSyntaxTreeNode) {
		let analyzerErrorsAndWarnings = SemanticAnalyzer.analyze(node)
		if let analyzerErrors = analyzerErrorsAndWarnings.errors {
			errors += analyzerErrors
		}
		if let analyzerWarnings = analyzerErrorsAndWarnings.warnings {
			warnings += analyzerWarnings
		}
	}

	func parseStatements(parentNode: MutableSyntaxTreeNode) throws {
		if atEndOfTokens() {
			throw parserError(.unexpectedEndOfTokens)
		}

		while true {
			let (statement, shouldContinue) = try parseStatement()
			if let statement = statement {
				parentNode.addStatement(statement)
			}
			if !shouldContinue {
				break
			}
		}
	}

	func parseStatement() throws -> (statement: SyntaxTreeNode?, shouldContinue: Bool) {

		guard let token = currentToken() else {
			return (nil, false)
		}

		if token.isBinaryOperation() || token.isPostfixOperation() {
			throw unexpectedTokenError()
		}

		var shouldContinue = true
		var statement: SyntaxTreeNode? = nil

		switch token.type {
		case .forToken:
			statement = try parseForLoop()
		case .whileToken:
			statement = try parseWhileLoop()
		case .ifToken:
			statement = try parseIf()
		case .tryToken:
			statement = try parseTry()
		case .block:
			statement = try parseBlock()
		case .breakToken:
			statement = try parseBreak()
		case .continueToken:
			statement = try parseContinue()
		case .varToken:
			statement = try parseVar()
		case .letToken:
			statement = try parseLet()
		case .def:
			statement = try parseDef()
		case .returnToken:
			statement = try parseReturn()
		default:
			statement = nil
		}

		if statement == nil {
			statement = try parseExpression()
		}

		if atEndOfTokens() {
			shouldContinue = false
		}

		moveAheadToken()
		if let updatedCurrentToken = currentToken(), updatedCurrentToken.type == .rightBrace {
			shouldContinue = false
			moveAheadToken()
	}

		return (statement, shouldContinue)
	}

	func parseForLoop() throws -> SyntaxTreeNode {
		throw unimplementedError()
	}

	func parseWhileLoop() throws -> SyntaxTreeNode {
		throw unimplementedError()
	}

	func parseIf() throws -> SyntaxTreeNode {
		throw unimplementedError()
	}

	func parseTry() throws -> SyntaxTreeNode {
		throw unimplementedError()
	}

	func parseBlock() throws -> SyntaxTreeNode {
		throw unimplementedError()
	}

	func parseReturn() throws -> SyntaxTreeNode {
		throw unimplementedError()
	}

	func parseVar() throws -> SyntaxTreeNode {
		throw unimplementedError()
	}

	func parseLet() throws -> SyntaxTreeNode {
		throw unimplementedError()
	}

	func parseDef() throws -> SyntaxTreeNode {
		throw unimplementedError()
	}

	func parseExpression() throws -> SyntaxTreeNode {
		throw unimplementedError()
	}

	// MARK: - Parse One-keyword statements

	func parseBreak() throws -> SyntaxTreeNode {
		guard currentTokenIsLastInStatement() else {
			throw parserError(.breakNotAloneInStatement)
		}
		return BreakNode(textPosition: currentToken()!.textPosition)
	}

	func parseContinue() throws -> SyntaxTreeNode {
		guard currentTokenIsLastInStatement() else {
			throw parserError(.continueNotAloneInStatement)
		}
		return ContinueNode(textPosition: currentToken()!.textPosition)
	}

	// MARK: - Cursor Position

	func atEndOfTokens() -> Bool {
		return currentIndex >= endIndex
	}

	@discardableResult
	func moveAheadToken() -> Bool {
		if atEndOfTokens() {
			return false
		}
		currentIndex = currentIndex + 1
		guard let _ = currentToken() else {
			return false
		}
		return true
	}

	func currentToken() -> Token? {
		if atEndOfTokens() {
			return nil
		}
		return tokens[currentIndex]
	}

	func popToken() -> Token? {
		guard let token = currentToken() else {
			return nil
		}
		moveAheadToken()
		return token
	}

	func nextToken() -> Token? {
		if atEndOfTokens() {
			return nil
		}
		let ix = currentIndex + 1
		if ix >= endIndex {
			return nil
		}
		return tokens[ix]
	}

	func currentTokenIsLastInStatement() -> Bool {
		guard let currentToken = currentToken() else {
			return false
		}
		guard let nextToken = nextToken() else {
			return true // If there’s no next token, this must be the last in a statement.
		}
		if nextToken.isEndOfStatementToken() {
			return true
		}
		// If next token is on a separate line, then this token is last in statement.
		return currentToken.textPosition.lineNumber != nextToken.textPosition.lineNumber
	}

	// MARK: - Errors

	func parserError(_ type: ParserError.ParserErrorType) -> ParserError {
		return ParserError(type, textPosition: currentToken()?.textPosition)
	}

	func unimplementedError() -> ParserError {
		return parserError(.unimplemented)
	}

	func unexpectedTokenError() -> ParserError {
		return parserError(.unexpectedToken)
	}
}

private extension Token {

	// These are always binary operations. There are some tokens that are *sometimes* binary operations.
	static let binaryOperationTokenTypes: [TokenType] = [.not, .and, .or, .equals, .notEquals, .greaterThan, .lessThan, .greaterThanEquals, .lessThanEquals, .trueToken, .falseToken, .assign, .star, .slash, .percent]

	func isBinaryOperation() -> Bool {
		return Token.binaryOperationTokenTypes.contains(type)
	}

	// These are always postfix operations.
	static let postfixOperationTokenTypes: [TokenType] = [.plusPlus, .minusMinus, .caret]

	func isPostfixOperation() -> Bool {
		return Token.postfixOperationTokenTypes.contains(type)
	}

	static let endOfStatementTokenTypes: [TokenType] = [.semicolon, .leftBrace, .rightBrace, .comment]
	
	func isEndOfStatementToken() -> Bool {
		return Token.endOfStatementTokenTypes.contains(type)
	}
}
