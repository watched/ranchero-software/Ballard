//
//  SynxtaxTreeNode+Evaluation.swift
//  Ballard
//
//  Created by Brent Simmons on 4/27/19.
//  Copyright © 2019 Ranchero Software. All rights reserved.
//

import Foundation

extension SyntaxTreeNode {

	func evaluateStatementList(_ statementList: StatementListNode, _ stack: Stack) throws -> EvaluationResult {

		// TODO: this will have to hook into the debugger.
		// We will need to be able to stop on a line of code and resume.
		// This could be tricky. We’ll think about it later.
		
		stack.push(self)
		defer {
			stack.pop()
		}

		return try evaluateNodes(statementList.statements, stack)
	}

	func evaluateNodes(_ nodes: [SyntaxTreeNode], _ stack: Stack) throws -> EvaluationResult {

		for node in nodes {
			let result = try node.evaluate(stack)

			switch result {
			case .emptyResult, .valueResult:
				continue
			case .breakResult, .continueResult, .returnResult:
				return result
			}
		}
		return .emptyResult
	}

	func value(from node: SyntaxTreeNode, _ stack: Stack) throws -> Value {
		let result = try node.evaluate(stack)
		switch result {
		case .valueResult(let value):
			return value
		case .returnResult(let value):
			return value
		default:
			throw RuntimeError(.evaluationFailure)
		}
	}
}
