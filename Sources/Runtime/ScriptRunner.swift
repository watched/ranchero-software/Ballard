//
//  ScriptRunner.swift
//  Ballard
//
//  Created by Brent Simmons on 4/27/19.
//  Copyright © 2019 Ranchero Software. All rights reserved.
//

import Foundation

// Runs a script that’s been compiled into an Abstract Syntax Tree (AST).
// A compiled script is a tree of structs that implement the SyntaxTreeNode protocol.
// Each node knows how to evaluate itself.


public enum ScriptRunnerResult {
	case success(Value?)
	case parserFailure(ParserResult)
	case runtimeFailure(RuntimeError)
}

public struct ScriptRunner {

	static func run(_ scriptNode: ScriptNode) -> ScriptRunnerResult {
		do {
			let stack = Stack()
			let evaluationResult = try scriptNode.evaluate(stack)
			return .success(evaluationResult.value())
		} catch let error as RuntimeError {
			return .runtimeFailure(error)
		} catch {
			preconditionFailure("Unexpected error in run.")
		}
	}

	static func runScriptText(_ scriptText: String) -> ScriptRunnerResult {
		let parserResult = Parser.parseScriptText(scriptText)
		if !parserResult.isEvaluatable() {
			return .parserFailure(parserResult)
		}
		return run(parserResult.firstNode! as! ScriptNode)
	}
}

