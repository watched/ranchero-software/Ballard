//
//  Stack.swift
//  Ballard
//
//  Created by Brent Simmons on 5/6/17.
//  Copyright © 2017 Ranchero Software. All rights reserved.
//

import Foundation

// Each scope gets a stack.
// Stacks are actually a tree structure:
// A single frame may have multiple children.
// However: a frame knows its parent, but not its children.

final class Stack {

	var frames = [StackFrame]()
	var currentFrame: StackFrame? {
		get {
			if frames.isEmpty {
				return nil
			}
			return frames.last
		}
	}

	@discardableResult
	func push(_ node: SyntaxTreeNode) -> StackFrame {
		let frame = StackFrame(node, currentFrame)
		frames += [frame]
		return frame
	}

	func pop() {
//		frames.pop()
	}
}
