//
//  Keywords.swift
//  Ballard
//
//  Created by Brent Simmons on 11/14/18.
//  Copyright © 2018 Ranchero Software. All rights reserved.
//

import Foundation

struct Keywords {

	private static let tokenLookup: [String: TokenType] = ["for": .forToken, "in": .inToken, "while": .whileToken, "if": .ifToken, "else": .elseToken, "try": .tryToken, "block": .block, "break": .breakToken, "continue": .continueToken, "return": .returnToken, "let": .letToken, "var": .varToken, "def": .def, "true": .trueToken, "false": .falseToken]

	static func token(for keyword: String) -> TokenType? {
		return tokenLookup[keyword]
	}
}
