//
//  EvaluationResult.swift
//  Ballard
//
//  Created by Brent Simmons on 4/27/19.
//  Copyright © 2019 Ranchero Software. All rights reserved.
//

import Foundation

enum EvaluationResult {
	case emptyResult
	case valueResult(Value)

	// Explicit return
	case returnResult(Value)

	// Used in loops
	case breakResult
	case continueResult

	func value() -> Value? {
		switch self {
		case .valueResult(let value):
			return value
		case .returnResult(let value):
			return value
		case .emptyResult, .breakResult, .continueResult:
			return nil
		}
	}
}

