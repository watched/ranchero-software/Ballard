//
//  RuntimeError.swift
//  Ballard
//
//  Created by Brent Simmons on 11/13/18.
//  Copyright © 2018 Ranchero Software. All rights reserved.
//

import Foundation

// Runtime errors

public struct RuntimeError: Error {

	public enum RuntimeErrorType {
		case coercionNotPossible
		case negativeValueNotPossible
		case notValueNotPossible
		case comparisonNotPossible
		case evaluationFailure
		case unexpectedOperation
	}

	public let errorType: RuntimeErrorType

	public init(_ errorType: RuntimeErrorType) {
		self.errorType = errorType
	}
}
