# Ballard

Language and runtime for [Rainier](https://github.com/brentsimmons/Rainier), a scripting system for Mac.

This scripting language is designed to be embeddable in other apps. For instance, we plan to use it in [NetNewsWire](https://github.com/brentsimmons/NetNewsWire) too.

We’re just getting started with this. The build is probably broken. It’s going to be broken for a long time.

In fact, if it’s not broken at any given moment, consider it pure coincidence.
