//
//  ErrorsAndWarnings.swift
//  Ballard
//
//  Created by Brent Simmons on 5/10/19.
//  Copyright © 2019 Ranchero Software. All rights reserved.
//

import Foundation

struct ErrorsAndWarnings {

	let errors: [ErrorWithTextPosition]?
	let warnings: [ErrorWithTextPosition]?
}
