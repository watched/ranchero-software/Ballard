//
//  ErrorWithTextPosition.swift
//  Ballard
//
//  Created by Brent Simmons on 5/4/19.
//  Copyright © 2019 Ranchero Software. All rights reserved.
//

import Foundation

protocol ErrorWithTextPosition: Error {
	
	var textPosition: TextPosition? { get }
}
